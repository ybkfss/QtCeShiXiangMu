﻿#include "sampdataformat.h"

DataHeader::DataHeader() :
    bfSize(0),
    diVer(100),
    diADBit(24),
    diSize(0),
    diSampleFreq(1024),
    diChannelNum(1),
    diCh(1),
    diSensitivity(1),
    diMultiple(0)
{
    sprintf(bfType, "VIPC");
    sprintf(diTestPointNum, "001");
    sprintf(diUnit, "mV");
    sprintf(diRemark, "remark");
    sprintf(diDataFlag, "data");
}

DataHeader::DataHeader(const char* type,
                       long fsize,
                       short ver,
                       short ADBit,
                       long dSzie,
                       double freq,
                       short chNum,
                       short ch,
                       float sen,
                       const char* pointNum,
                       int multiple,
                       const char* unit,
                       const char* remark,
                       const char*  flag) :
    bfSize(fsize),
    diVer(ver),
    diADBit(ADBit),
    diSize(dSzie),
    diSampleFreq(freq),
    diChannelNum(chNum),
    diCh(ch),
    diSensitivity(sen),
    diMultiple(multiple)
{
    memcpy(bfType, type, 4);
    memcpy(diTestPointNum, pointNum, 10);
    memcpy(diUnit, unit, 20);
    memcpy(diRemark, remark, 256);
    memcpy(diDataFlag, flag, 4);
}

bool DataHeader::operator==(const DataHeader &other) const
{
    return bfType == other.bfType && bfSize == other.bfSize &&
            diVer == other.diVer && diADBit == other.diADBit &&
            diSize == other.diSize && diChannelNum == other.diChannelNum &&
            diCh == other.diCh && diTestPointNum == other.diTestPointNum &&
            diMultiple == other.diMultiple && diUnit == other.diUnit &&
            diRemark == other.diRemark && diDataFlag == other.diDataFlag;
}

SampDataFormat::SampDataFormat() : HeaderSize(326), Data(nullptr)
{

}

SampDataFormat::~SampDataFormat()
{
    if(Data != nullptr) {
        delete [] Data;
        Data = nullptr;
    }
}

bool SampDataFormat::ReadDataFile(const QString &FilePath)
{
    QFile file(FilePath);
    if(!file.open(QIODevice::ReadOnly)) {
        return false;
    }
    QByteArray da = file.read(HeaderSize);
    memcpy(Header.bfType, da.data(), 4);
    memcpy(&Header.bfSize, da.data() + 4, sizeof(long));
    memcpy(&Header.diVer, da.data() + 8, sizeof(short));
    memcpy(&Header.diADBit, da.data() + 10, sizeof(short));
    memcpy(&Header.diSize, da.data() + 12, sizeof(long));
    memcpy(&Header.diSampleFreq, da.data() + 16, sizeof(double));
    memcpy(&Header.diChannelNum, da.data() + 24, sizeof(short));
    memcpy(&Header.diCh, da.data() + 26, sizeof(short));
    memcpy(Header.diTestPointNum, da.data() + 28, 10);
    memcpy(&Header.diSensitivity, da.data() + 38, sizeof(float));
    memcpy(&Header.diMultiple, da.data() + 42, sizeof(int));
    memcpy(Header.diUnit, da.data() + 46, 20);
    memcpy(Header.diRemark, da.data() + 66, 256);
    memcpy(Header.diDataFlag, da.data() + 322, 4);
    if(Data == nullptr)
        Data = new float[static_cast<uint>(Header.diSize * 4)];
    da = file.read(Header.diSize * 4);
    memcpy(Data, da.data(), static_cast<uint>(Header.diSize * 4));
    file.close();
    return true;
}
