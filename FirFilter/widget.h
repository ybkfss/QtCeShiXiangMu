﻿#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QComboBox>
#include <QLineEdit>
#include "sampdataformat.h"
#include "singlechaanle.h"
#include "filter.h"
#include "matrix.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);
    ~Widget();

private slots:
    void on_BtnReadFile_clicked();

    void BtnCalc_clicked();

    void cBoxFilterType_curIndexChanged(int index);

    void cBoxWinType_curIndexChanged(int index);

    void EditScala_editingFinished();

    void EditFreq_editingFinished();

    void EditLowerFreq_editingFinished();

    void EditUpperFreq_editingFinished();

private:
    Ui::Widget *ui;
    SingleChaanle* WaveIn;          // 输入波形
    SingleChaanle* WaveInSignal;    // 输入信号频谱
    SingleChaanle* WaveImpuls;      // 冲击响应
    SingleChaanle* WaveFreq;        // 频率响应
    SingleChaanle* WaveOut;         // 输出波形
    SingleChaanle* WaveSignalOut;   // 输出信号频谱
    QLabel* LabFilterType;
    QComboBox* cBoxFilterType;
    QHBoxLayout* hLayoutFilter;
    QLabel* LabWinType;
    QComboBox* cBoxWinType;
    QHBoxLayout* hLayoutWin;
    QLabel* LabScala;
    QLineEdit* EditScala;
    QHBoxLayout* hLayoutScala;
    QLabel* LabFreq;
    QLineEdit* EditFreq;
    QHBoxLayout* hLayoutFreq;
    QLabel* LabUpperFreq;
    QLineEdit* EditUpperFreq;
    QHBoxLayout* hLayoutUpperFreq;
    QLabel* LabLowerFreq;
    QLineEdit* EditLowerFreq;
    QHBoxLayout* hLayoutLowerFreq;
    QPushButton* BtnCalcWave;
    QVBoxLayout* vLayoutWave;
    QVBoxLayout* vLayoutSignal;
    QVBoxLayout* vLayoutOp;
    QHBoxLayout* hLayoutMain;
    SampDataFormat SampData;
    // 参数
    int FilterType;
    int WinType;
    int Scala;
    double Freq;
    double LowerFreq;
    double UpperFreq;
};

#endif // WIDGET_H
