﻿#include "widget.h"
#include "ui_widget.h"
#include "dxfreader.h"
#include <QGraphicsLineItem>
#include <QCoreApplication>
#include <QFileDialog>
#include <QDebug>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
    , scene(new QGraphicsScene)
    , View(new JGraphicsView)
    , btnOpen(new QPushButton("..."))
    , hLayMain(new QHBoxLayout)
{
    ui->setupUi(this);

    // layout
    hLayMain->addWidget(View);
    hLayMain->addWidget(btnOpen);
    setLayout(hLayMain);

    // setting
    hLayMain->setStretch(0, 90);
    hLayMain->setStretch(1, 10);
    View->setScene(scene);
    View->setRenderHint(QPainter::Antialiasing, false);
//    View->setDragMode(QGraphicsView::RubberBandDrag);
    View->setOptimizationFlags(QGraphicsView::DontSavePainterState);
    View->setViewportUpdateMode(QGraphicsView::SmartViewportUpdate);
    View->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);

    // connect
    connect(btnOpen, &QPushButton::clicked, this, &Widget::btnOPen_clicked);

    // 坐标线
//    QPen pen;
//    pen.setWidth(0);
//    pen.setColor(QColor(0, 0, 0));
//    QGraphicsLineItem* LineX = new QGraphicsLineItem;
//    QLineF p;
//    p.setP1(LineX->mapFromScene(QPointF(-100000, 0)));
//    p.setP2(LineX->mapFromScene(QPointF(100000, 0)));
//    LineX->setLine(p);
//    LineX->setPen(pen);
//    scene->addItem(LineX);
//    QGraphicsLineItem* LineY = new QGraphicsLineItem;
//    p.setP1(LineY->mapFromScene(QPointF(0, 100000)));
//    p.setP2(LineY->mapFromScene(QPointF(0, -100000)));
//    LineY->setLine(p);
//    LineY->setPen(pen);
//    scene->addItem(LineY);
}

Widget::~Widget()
{
    scene->deleteLater();
    View->deleteLater();
    btnOpen->deleteLater();
    hLayMain->deleteLater();
    delete ui;
}

void Widget::btnOPen_clicked()
{
#if 0
    QString fileName = QFileDialog::getOpenFileName(this, QString::fromLocal8Bit("选择 dxf 文件"), "", "*.dxf");
    DxfReader reader(fileName);
    QPen pen;
    pen.setWidth(0);
    pen.setColor(QColor(Qt::red));
    qreal XOffset(20404000);
    qreal YOffset(3918000);
    bool isCental = false;
    for(auto item: reader.dxfLines) {
        QGraphicsLineItem* Line = new QGraphicsLineItem;
        QLineF p;
        p.setP1(Line->mapFromScene(QPointF(item.x1 - XOffset, YOffset - item.y1)));
        p.setP2(Line->mapFromScene(QPointF(item.x2 - XOffset, YOffset - item.y2)));
        Line->setLine(p);
        Line->setPen(pen);
        scene->addItem(Line);
        if(!isCental) {
            View->centerOn(item.x1 - XOffset, YOffset - item.y1);
            isCental = true;
        }
    }
    for(auto item: reader.dxfText) {
        QString text = item.text.data();
//        text.replace("%%D", "°");
        int x = static_cast<int>(item.ipx - XOffset);
        int y = static_cast<int>(YOffset - item.ipy - 13);
        JGraphicsTextItem *Text = new JGraphicsTextItem(text, x, y);
        Text->setRotation(-item.angle);
        scene->addItem(Text);
        QCoreApplication::processEvents();
    }
#else
    QPen pen;
    pen.setWidth(0);
    pen.setColor(QColor(0, 0, 0));
    QGraphicsLineItem* LineX = new QGraphicsLineItem;
    QLineF p;
    p.setP1(LineX->mapFromScene(QPointF(-100000, 0)));
    p.setP2(LineX->mapFromScene(QPointF(100000, 0)));
    LineX->setLine(p);
    LineX->setPen(pen);
    scene->addItem(LineX);
    QGraphicsLineItem* LineY = new QGraphicsLineItem;
    p.setP1(LineY->mapFromScene(QPointF(0, 100000)));
    p.setP2(LineY->mapFromScene(QPointF(0, -100000)));
    LineY->setLine(p);
    LineY->setPen(pen);
    scene->addItem(LineY);

    int y =  -2000;
    for (int i = 0; i < 100; i++) {
        int x = -2000;
        QString text = QString::fromLocal8Bit("5310工作面");
        for (int j = 0; j < 50; j++) {
            x += 100;
            JGraphicsTextItem *item1 = new JGraphicsTextItem(text, x, y);
//            item1->setRotation(-20);
            scene->addItem(item1);
        }
        y += 20;
    }
#endif
}
