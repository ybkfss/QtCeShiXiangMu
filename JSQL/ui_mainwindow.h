/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.10.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionMySql;
    QAction *actionSQL_Server;
    QAction *actionOpen_Connection;
    QAction *actionClose_Connection;
    QAction *actionExport_Connect;
    QAction *actionImport_Connection;
    QAction *actionClose;
    QAction *actionExit;
    QWidget *centralWidget;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuNew_Connection;
    QMenu *menuCheck;
    QMenu *menuFavourite;
    QMenu *menuTools;
    QMenu *menuWindows;
    QMenu *menuHelp;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(887, 550);
        actionMySql = new QAction(MainWindow);
        actionMySql->setObjectName(QStringLiteral("actionMySql"));
        actionSQL_Server = new QAction(MainWindow);
        actionSQL_Server->setObjectName(QStringLiteral("actionSQL_Server"));
        actionOpen_Connection = new QAction(MainWindow);
        actionOpen_Connection->setObjectName(QStringLiteral("actionOpen_Connection"));
        actionClose_Connection = new QAction(MainWindow);
        actionClose_Connection->setObjectName(QStringLiteral("actionClose_Connection"));
        actionExport_Connect = new QAction(MainWindow);
        actionExport_Connect->setObjectName(QStringLiteral("actionExport_Connect"));
        actionImport_Connection = new QAction(MainWindow);
        actionImport_Connection->setObjectName(QStringLiteral("actionImport_Connection"));
        actionClose = new QAction(MainWindow);
        actionClose->setObjectName(QStringLiteral("actionClose"));
        actionExit = new QAction(MainWindow);
        actionExit->setObjectName(QStringLiteral("actionExit"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 887, 23));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuNew_Connection = new QMenu(menuFile);
        menuNew_Connection->setObjectName(QStringLiteral("menuNew_Connection"));
        menuCheck = new QMenu(menuBar);
        menuCheck->setObjectName(QStringLiteral("menuCheck"));
        menuFavourite = new QMenu(menuBar);
        menuFavourite->setObjectName(QStringLiteral("menuFavourite"));
        menuTools = new QMenu(menuBar);
        menuTools->setObjectName(QStringLiteral("menuTools"));
        menuWindows = new QMenu(menuBar);
        menuWindows->setObjectName(QStringLiteral("menuWindows"));
        menuHelp = new QMenu(menuBar);
        menuHelp->setObjectName(QStringLiteral("menuHelp"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuCheck->menuAction());
        menuBar->addAction(menuFavourite->menuAction());
        menuBar->addAction(menuTools->menuAction());
        menuBar->addAction(menuWindows->menuAction());
        menuBar->addAction(menuHelp->menuAction());
        menuFile->addAction(menuNew_Connection->menuAction());
        menuFile->addAction(actionOpen_Connection);
        menuFile->addAction(actionClose_Connection);
        menuFile->addSeparator();
        menuFile->addAction(actionExport_Connect);
        menuFile->addAction(actionImport_Connection);
        menuFile->addSeparator();
        menuFile->addAction(actionClose);
        menuFile->addAction(actionExit);
        menuNew_Connection->addAction(actionMySql);
        menuNew_Connection->addAction(actionSQL_Server);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "JSQL", nullptr));
        actionMySql->setText(QApplication::translate("MainWindow", "MySql", nullptr));
        actionSQL_Server->setText(QApplication::translate("MainWindow", "SQL Server", nullptr));
        actionOpen_Connection->setText(QApplication::translate("MainWindow", "Open Connection", nullptr));
        actionClose_Connection->setText(QApplication::translate("MainWindow", "Close Connection", nullptr));
        actionExport_Connect->setText(QApplication::translate("MainWindow", "Export Connection", nullptr));
        actionImport_Connection->setText(QApplication::translate("MainWindow", "Import Connection", nullptr));
        actionClose->setText(QApplication::translate("MainWindow", "Close", nullptr));
        actionExit->setText(QApplication::translate("MainWindow", "Exit JSQL", nullptr));
        menuFile->setTitle(QApplication::translate("MainWindow", "File", nullptr));
        menuNew_Connection->setTitle(QApplication::translate("MainWindow", "New Connection", nullptr));
        menuCheck->setTitle(QApplication::translate("MainWindow", "Check", nullptr));
        menuFavourite->setTitle(QApplication::translate("MainWindow", "Favourite", nullptr));
        menuTools->setTitle(QApplication::translate("MainWindow", "Tools", nullptr));
        menuWindows->setTitle(QApplication::translate("MainWindow", "Windows", nullptr));
        menuHelp->setTitle(QApplication::translate("MainWindow", "Help", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
